import numpy as np
import scipy.optimize as optimise
import numpy.random as random
import math
from scipy.stats import cosine
from typing import NamedTuple

class X_Ycoordinate(NamedTuple):
    x: float
    y: float
        
        
class traj_vector(NamedTuple):
    theta: float
    phi: float
        
        
class elec_state(NamedTuple):
    E: float
    t: float

        
class number_gen:
    norm = 1.
    
    def __init__(self, func, domain):
        self.func = func
        self.domain = domain

        
    def normalise(self):
        domain_vals = np.linspace(*self.domain, 1000)
        range_vals = np.empty(1000)
        for i in range(1000):
            range_vals[i] = self.func(domain_vals[i])
        max_range_val = np.amax(range_vals)
        self.norm = 1/max_range_val
        
        
    def dice_roll(self):
        while True:
            x = random.uniform(*self.domain)
            y = self.func(x) * self.norm
            diceroll = random.rand()
            if diceroll < y:
                return x

            


    
def intensity(wav):
    h = 6.626e-34
    c = 3e8
    k = 1.38e-23
    a = 2.0*h*c**2
    b = h*c/(wav*k*T)
    return a/ ( (wav**5) * (np.exp(b) - 1.0) )
            
        


#def blackbody_diceroll(upperwav = 1e-9, lowerwav = 3e-6, T=5000.):
#    """Samples a random number based on a blackbody distribution of temperature
#    T.
#    ---
#    Parameters:
#    upperwav (opt.): Float. Limits minimum wavelength return value. Default
#    to 1 nm
#    lowerwav (opt): Float. Limits maximum wavelength return value. Default
#    to 3 micro-m
#    T: Float. Blackbody temperature. Default to 5000 K
#    ---
#    Returns:
#    wav: Float. A blackbody distributed random wavelength
#    """
#    peakwav = 2.898e-3/T
#    h = 6.626e-34
#    c = 3e8
#    k = 1.38e-23
#    a = 2.0*h*c**2
#    bpeak = h*c/(peakwav*k*T)   
#    
#    max_intensity = a/ ( (peakwav**5) * (np.exp(bpeak) - 1.0) )
#    
#    while True:
#        wav = random.uniform(lowerwav, upperwav)
#        b = h*c/(wav*k*T)
#        
#        intensity = a/ ( (wav**5) * (np.exp(b) - 1.0) )
#        normed_intensity = intensity / max_intensity
#        
#        dice_roll = random.rand()
#        
#        if dice_roll < normed_intensity:
#            break
#    return wav


#def cos_6_diceroll(lower = 0, upper = math.pi/2):
   # """Samples a random number based on a cos^6 distribution.
   # ---
  # Parameters: 
    #lower (opt.): Float. Minimum angle in sample range. Radians
    #upper (opt.): Float. Maximum angle in sample range. Radians
   # ---
    #Returns: theta. Float"""
    
  #  while True:
 #       theta = random.uniform(lower, upper)
 #       cos_six = np.cos(theta)**6
  #      dice_roll = random.rand()
        
  #      if dice_roll < cos_six:
  #          break      
  #  return theta


def max_boltz_E(kT, lower=0, upper = 1):
    """Returns a maxwell distrubed random energy at a equillibrium
    temperature T.
    ---
   Parameterss:
    -E: float eV
    -kT: float eV
    Returns: float. Energy eV."""
    
    while True:
        E = random.uniform(lower, upper)
        p = np.exp(-E/kT)/np.exp(-1)
        if random.rand() < p:
            break
        return E


def collimatedlightfunc(lightdim):
    """Returns a single, random, uniformly selected coordinate (x,y) within a 
    defined rectangular region.
    ---
    Parameters:
    -lightdim: 2x2 array [[a,b],[c,d]] where a,b are boundary coordinates (floats)
    in x-axis and c,d are boundary coordinates in y-axis
    ---
    Returns:
    X_Ycoordinate: Tuple. (x,y)"""
    
    x = random.uniform(lightdim[0, 0], lightdim[0, 1])
    y = random.uniform(lightdim[1, 0], lightdim[1, 1])
    return X_Ycoordinate(x, y)


def MCtest_collimated(lightdim, temperature, detectordim, d, trials = 10000,
                     upperwav = 1e-9, lowerwav = 3e-6):
    """Carries out a flat screen detection of photons emitted by a light
    source onto a detector screen. Assumes rectangular geometry.
    ---
    Parameters: 
    - lightdim: 2x2 array [[a,b],[c,d]] 
    - detetctordim: 2x2 array [[a,b,[c,d]]
    a,b are boundary coordinates (floats)
    in x-axis and c,d are boundary coordinates in y-axis
    - d: separation distance between source and detector
    - trials (opt.): int. Sets the number of trials. Default is 10000
    - upperwav (opt.): Float. Sets upper limit of wavelength sampling.
    Default is 1 nm.
    - lowerwav (opt.): Float. Sets lower limit of wavelength sampling
    Default is 3 microns
    ---
    Returns:
    - 4xn array where n is the number of trials. 3rd column takes on a 
    boolean value. True if hits detector. False if not. 4th column gives 
    wavelength float values
    """
    
    def intensity(wav):
        T = temperature
        h = 6.626e-34
        c = 3e8
        k = 1.38e-23
        a = 2.0*h*c**2
        b = h*c/(wav*k*T)
        return a/ ( (wav**5) * (np.exp(b) - 1.0) )
    
    blackbody = number_gen(intensity, (lowerwav, upperwav))
    blackbody.normalise()       
    
    data = np.zeros((4, trials))
    
    for i in range(trials):
        X_Ycoordinate = collimatedlightfunc(lightdim)
        wavelength = blackbody.dice_roll()
        if detectordim[0,0] < X_Ycoordinate.x < detectordim[0,1] and detectordim[1,0] < X_Ycoordinate.y < detectordim[1,1]:
            hittest = True
        else:
            hittest = False

        data[:,i] = *X_Ycoordinate, hittest, wavelength
        
    return data


def lambertianlightfunc(lightdim):
    """Returns a single, random, uniformly selected coordinate (x,y) within a 
    defined rectangular region with a lambertian angle theta (cosine distro) and
    phi (uniform).
    ---
    Parameters:
    -lightdim: 2x2 array [[a,b],[c,d]] where a,b are boundary coordinates (floats)
    in x-axis and c,d are boundary coordinates in y-axis
    ---
    Returns:
    lambertian_vector: Tuple. (x, y, theta, phi)"""
    
    x = random.uniform(lightdim[0, 0], lightdim[0, 1])
    y = random.uniform(lightdim[1, 0], lightdim[1, 1])
    phi = random.uniform(0, 2*math.pi)
    theta = cosine.rvs()
    return X_Ycoordinate(x,y), traj_vector(theta,phi)


def lambertiantrajectory(X_Ycoordinate, traj_vector, d):
    """Determines the trajectory of a photon from an initial lambertian state
    ---
    Parameters:
    XY_coordinate: Tuple. (x, y)
    traj_vector: Tuple. (theta, phi)
    d: float. distance between screen and light source
    ---
    Returns:
    X_Ycoordinates: Tuple"""
    
    r = d*math.tan(traj_vector.theta)
    
    x = X_Ycoordinate.x + r*math.cos(traj_vector.phi)
    y = X_Ycoordinate.y + r*math.sin(traj_vector.phi)
    
    return (x, y)


def MCtest_lambertian(lightdim, temperature, detectordim, d, trials = 10000,
                     upperwav = 1e-9, lowerwav = 3e-6):
    """Carries out a flat screen detection of photons emitted by a light
    source with lambertian characteristic. Assumes rectangular geometry.
    ---
    Parameters: 
    - lightdim: 2x2 array [[a,b],[c,d]] 
    - detetctordim: 2x2 array [[a,b,[c,d]]
    a,b are boundary coordinates (floats)
    in x-axis and c,d are boundary coordinates in y-axis 
    - d: separation distance between source and detector
    - trials (opt.): int. Sets the number of trials. Default is 10000
    - upperwav (opt.): Float. Sets upper limit of wavelength sampling.
    Default is 1 nm.
    - lowerwav (opt.): Float. Sets lower limit of wavelength sampling
    Default is 3 microns
    ---
    Returns:
    - 4xn array where n is the number of trials. 3rd column takes on a 
    boolean value. True if hits detector. False if not. 4th column gives 
    wavelength float values"""
    
    def intensity(wav):
        T =temperature
        h = 6.626e-34
        c = 3e8
        k = 1.38e-23
        a = 2.0*h*c**2
        b = h*c/(wav*k*T)
        return a/ ( (wav**5) * (np.exp(b) - 1.0) )
    
    blackbody = number_gen(intensity, (lowerwav, upperwav))
    blackbody.normalise()       
    
    data = np.zeros((4, trials))
    
    for i in range(trials):
        X_Ystart, traj_vector = lambertianlightfunc(lightdim)
        wavelength = blackbody.dice_roll()
        X_Ycoordinate = lambertiantrajectory(X_Ystart, traj_vector, d)
    
        if detectordim[0,0] < X_Ycoordinate[0] < detectordim[0,1] and detectordim[1,0] < X_Ycoordinate[1] < detectordim[1,1]:
            hittest = True
        else:
            hittest = False
            
        data[:,i] = *X_Ycoordinate, hittest, wavelength
        
    return data


def photocathode(cathdim, E_min, E_max):
    """Returns a single, random, uniformly selected coordinate (x,y) within a 
    defined rectangular region with a theta angle to the surface normal, distributed
    as a cos^6 and a uniformly random phi angle. Also returns an energy (eV) 
    distributed uniformly.
    ---
    Parameters:
    -cathdim: 2x2 array [[a,b],[c,d]] where a,b are boundary coordinates (floats)
    in x-axis and c,d are boundary coordinates in y-axis.
    -maxwell_temp: float. equillibrium temperature. Kelvin
    -E_min
    -E_max
    ---
    Returns:
    electron_state: Tuple. (x, y, theta, phi, E)"""
    x = random.uniform(cathdim[0, 0], cathdim[0, 1])
    y = random.uniform(cathdim[1, 0], cathdim[1, 1])
    phi = random.uniform(0, 2*math.pi)
    cos_6 = number_gen(lambda x: np.cos(x)**6, (0, np.pi/2))
    theta = cos_6.dice_roll()
    E = random.uniform(E_min, E_max)
    t = 0
            
    return X_Ycoordinate(x, y), traj_vector(theta, phi), elec_state(E, t)


def photocathode2(cathdim, E_rad_average):
    """"""
    x = random.uniform(cathdim[0, 0], cathdim[0, 1])
    y = random.uniform(cathdim[1, 0], cathdim[1, 1])
    phi = random.uniform(0, 2*math.pi)
    cos_6 = number_gen(lambda x: np.cos(x)**6, (0, np.pi/2))
    theta = cos_6.diceroll()
    max_boltz_E = number_gen(
    t = 0
    
    return X_Ycoordinate(x, y), traj_vector(theta, phi), elec_state(E_radial, t)


def photocathode_gap(X_Ycoordinate, traj_vector, elec_state, photocathode_sep = 1, photocathode_voltage = -3200, MCPin_voltage = -3000):
    """Takes the state of the electron after emission from the photocathode and 
    calculates the new state at MCPin"""
    q_m = 9.109/1.602 * 10**11
    theta = traj_vector.theta
    phi = traj_vector.phi
    speed_u = np.sqrt(2*elec_state.E*q_m)*1e-3
    speed_u_z = speed_u*np.cos(theta)
    speed_u_r = speed_u_z*np.sin(theta)
    speed_u_x = speed_u_r*np.cos(phi)
    speed_u_y = speed_u_r*np.sin(phi)
    
    E_gain = MCPin_voltage - photocathode_voltage
    E = E_gain + elec_state[0]
    speed_v = np.sqrt(2*E*q_m)*1e-3
    speed_v_z = np.sqrt(speed_v**2 - speed_u_r**2)
    
    t = elec_state.t
    t += 2*photocathode_sep/(speed_v_z + speed_u_z)
    
    x = X_Ycoordinate.x + t*(speed_u_x)
    y = X_Ycoordinate.y + t*(speed_u_y)
    
    theta = np.arccos(speed_v_z/speed_v)
    
    return (x, y), (theta, phi), (E, t)


def photocathode_gap2(X_Ycoordinate, traj_vector, elec_state, photocathode_sep = 1, photocathode_voltage = -3200, MCPin_voltage = -3000):
    """Takes the state of the electron after emission from the photocathode and 
    calculates the new state at MCPin"""
    q_m = 9.109/1.602 * 10**11
    V = MCPin_voltage - photocathode_voltage
    E = V + elec_state.E
    t = elec_state.t
    t += photocathode_sep*np.sqrt(2/(q_m*V))
    
    radial_shift = 2*photocathode_sep*np.sqrt(elec_state.E/V)
    phi = traj_vector.phi
    x = X_Ycoordinate.x + radial_shift*np.cos(phi)
    y = X_Ycoordinate.y + radial_shift*np.sin(phi)
    
    theta = np.arctan(np.sqrt(elec_state[0]/V))
    
    return (x, y), (theta, phi), (E, t)


def MCPin_surface(X_Ycoordinate, efficiency = 0.6):
    """"""
    diceroll = random.rand()
    if diceroll < efficiency:
            return True
           
