import csv
import math
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as optimise
import numpy.random as random
from typing import NamedTuple
from scipy import interpolate
from scipy.stats import cosine
from scipy.stats import rayleigh


class number_gen:
    """A class for generation of random numbers from arbitrary functions.
    ------
    Parameters
    ------
    func : a function with a return value
    domain : tuple
            Specifies the (min, max) values for sampling. Must be chosen
            so that func isn't asymptotic in range
            
    Attributes
    ------
    norm : float
            A normalisation factor for the arbitrary function
    """
    norm = 1.
    
    def __init__(self, func, domain):
        self.func = func
        self.domain = domain
        
    def normalise(self):
        """Sets norm so that dice_roll() outputs between 0,1. 
        This standardises any possible function for generating 
        random numbers from the dice_roll() method.
        """
        domain_vals = np.linspace(*self.domain, 1000)
        range_vals = np.empty(1000)
        for i in range(1000):
            range_vals[i] = self.func(domain_vals[i])
        max_val = np.amax(range_vals)
        self.norm = 1/max_val
    
    def dice_roll(self, data_size = 1):
        """
        Method to generate random numbers according to func
        distribution
        ------
        Parameters
        ------
        data_size : int. 
            Sets the length of output data array containing func
            distributed numbers.
        
        Returns
        ------
        1d numpy array 
        
        """
        data = np.empty(data_size)
        for i in range(data_size):
            while True:
                x = random.uniform(*self.domain)
                y = self.func(x) * self.norm
                diceroll = random.rand()
                if diceroll < y:
                    break
            data[i] = x
        return data   

                                                                                   
class particle:
    """A class for generating particle objects with position, 
    trajectory,energy, speed etc.
    ------
    Parameters
    ------
    x, y : float, optional
        position in x, y directions in millimetres mm.
    t : float, optional
        time 'coordinate' measured in s.
    theta, phi : float, optional
        angles in radians denoting the particle's trajectory.
    energy : float, optional
        particle energy measured in eV.
    speed : float 
        particle speed measured in m/s.
    
    Notes
    ------
    A particle is initialised by default with attribute .exist = False. 
    Can use plate and gap class methods to determine/affect assigned boolean 
    value. The attribute impacts whether a particle object
    is affected by other module objects such as gaps, plates and spotlights.
    """
    def __init__(self, x = 0, y = 0, theta = 0, phi = 0, t=0, energy = 0, 
                 speed = 0):
        self.x = x
        self.y = y
        self.t = t
        self.theta = theta
        self.phi = phi
        self.energy = energy
        self.speed = speed
        self.exist = False        
        
        
class photon(particle):
    """Light particle. 
    Assigning a wavelength will automatically assign an energy in eV.
    
    Parameters
    ------
    wavelength : float
        measured in nm.
        
    Attributes
    ------
    h : float
        Planck's constant measured in eVs
    c : float
        Speed of light defined in m/s
    """
    h = 4.136e-6
    c = 3.0e8
    def __init__(self, x = 0, y = 0, theta =0, phi = 0, t=0, energy = 0, 
                 speed = 0, wavelength=0):
        super().__init__(x, y, theta, phi, t, energy)
        self.wavelength = wavelength
        if wavelength != 0:
            self.energy = self.h*self.c/self.wavelength
            
            
class electron(particle):
    """Quantised charge particle. 
    
    Attrbutes
    ------
    mass : float
        defined in terms of kg.
    charge : float
        defined in terms of coulombs.
    """
    mass = 9.1e-31
    charge = 1.6e-19
    def __init__(self, x = 0, y = 0, theta = 0, phi = 0, t= 0, energy = 0,
                 speed = 0):
        super().__init__(x, y, theta, phi, t, energy)
        
        
class plate:
    """Class for generating plate objects with characteristic attributes 
    which can store information on colliding particles.

    Parameters
    ------
    dimension_array : numpy_array 2x2 
        Defines x, y geometry of the plate in terms of millimetres mm.
    ---
    """
    def __init__(self, dimension_array):
        self.dimensions = dimension_array
        self.x_min = dimension_array[0,0]
        self.x_max = dimension_array[0,1]
        self.y_min = dimension_array[1,0]
        self.y_max = dimension_array[1,1]
        self.x_coords = []
        self.y_coords = []
        self.time = []
        self.wavelength_vals = []
        self.energies = []
        
    def particle_hitloc(self, particle):
        """Records the hit location, time, energy and (where appropriate)
        wavelength of particle incident on a plate.
        Particle should have its attributes defined with standard units 
        as outlined in their class docstrings.
        
        Parameters
        ------
        particle : a particle object
        """
        if particle.exist == True:
            if (self.x_min < particle.x < self.x_max and 
                self.y_min < particle.y < self.y_max):
                
                self.x_coords.append(particle.x)
                self.y_coords.append(particle.y)
                self.time.append(particle.t)
                self.energies.append(particle.energy)
                if hasattr(particle,'wavelength'):
                    self.wavelength_vals.append(particle.wavelength)
                else:
                    self.wavelength_vals.append(0)
            
    def spacial_distribution(self, bins=50, **kwargs):
        """Displays a x, y distribution of recorded particle_hit_loc 
        on the plate as a 2-d colour histogram from matplotlib.pyplot.
        
        Parameters
        ------
        bins : float, optional
        """
        hist, xedge, yedge = np.histogram2d(self.x_coords, 
                                            self.y_coords, 
                                            bins)
        plt.figure()
        plt.imshow(hist, extent = [self.x_min, self.x_max,
                          self.y_min, self.y_max], **kwargs)
        plt.colorbar()
        plt.show()
        
    def wav_distribution(self, bins = 50, **kwargs):
        """Displays the distribution of wavelengths of recorded particle_hit_loc
        as a 1-d histogram from from matplotlib.pyplot.
        
        Parameters
        ------
        bins : float, optional
        """
        plt.figure()
        plt.hist(self.wavelength_vals, bins, **kwargs)
        plt.show()
        
    def energies_distribution(self, bins = 50, **kwargs):
        """Displays the energy distribution of recorded particle_hit_loc 
        as a 1-d histogram from from matplotlib.pyplot.
        
        Parameters
        ------
        bins : float, optional
        """
        plt.figure()
        plt.hist(self.energies, bins, **kwargs)
        plt.show()
        
    def clear(self):
        """Clears all data of particle_hit_loc on the plate.
        """
        self.x_coords = []
        self.y_coords = []
        self.time = []
        self.wavelength_vals = []
        self.energies = []
        
    def collimated_disperse(self, particle):
        """A method for assigning an x and y position of a particle object 
        collimated from the plate as a source. The particle object is assumed 
        to be with default attributes.
        
        Parameters
        ------
        particle : a particle object
        """
        particle.x = random.uniform(self.x_min, self.x_max)
        particle.y = random.uniform(self.y_min, self.y_max)
        self.x_coords.append(particle.x)
        self.y_coords.append(particle.y)
        particle.exist = True
        
    def lambertian_disperse(self, particle):
        """A method for assigning an x,y position and theta, phi trajectory of 
        a particle object, lambertian dispersed from the plate as a source. 
        The particle object is assumed to be with default attributes.
        
        Parameters
        ------
        particle : a particle object
        """
        particle.x = random.uniform(self.x_min, self.x_max)
        particle.y = random.uniform(self.y_min, self.y_max)
        particle.phi = random.uniform(0, 2*math.pi)
        particle.theta = cosine.rvs()
        self.x_coords.append(particle.x)
        self.y_coords.append(particle.y)
        particle.exist = True
        
        
class light_source(plate):
    """A plate which can carry a characteristic wavelength and/or temperature.
    
    Parameters
    ------
    wavelength : float, optional
        wavelength defined in terms of nanometres
    temperature : float, optional
        temperature in Kelvin.
    """
    
    def __init__(self, dimension_array, wavelength = None, temperature = None):
        super().__init__(dimension_array)
        self.wavelength = wavelength
        self.temperature = temperature
    
    def blackbody_dist(self, blackbody):
        """Method which assigns the light_source an attributed wavelength_select
        - a blackbody distribution of wavelengths.
        The attribute is produced using the number_gen class.

        Parameter
        ------
        blackbody: a blackbody class object defined by a temperature T, 
        with wavelength in nanometres.
        """
        self.wavelength_select = number_gen(blackbody.intensity, blackbody.domain)
        self.wavelength_select.normalise()
        
    def blackbody_wav(self, photon):
        """Method to assign a photon emitted from the light_source an attributed 
        wavelength according to wavelength_select. Defines photon.wavelength in 
        terms of nanometres.
        """
        photon.wavelength = float(self.wavelength_select.dice_roll())
        photon.energy = photon.h * photon.c/photon.wavelength
        self.wavelength_vals.append(photon.wavelength)
        
    
class photocathode(plate):
    """A plate with a work function and charcteristic quantum efficiency.
    
    Attributes
    ------
    work_func : float
        The work function defined in eV
    """
    work_func = 3. * 4.136*10/65
    
    def __init__(self, dimension_array):
        super().__init__(dimension_array)
    
    def quantum_efficiency(self, photon):
        """Method for returning the plate's quantum efficiency for a given
        photon.
        
        Parameters:
        ------
        photon : a photon object
        
        Notes
        ------
        This method is default at 100% efficiency it is useful to overwrite with
        .read_response method.  
        """
        return 1.
        
    def max_dist(self, max_boltz):
        """Method which assigns the photocathode an energy_select attribute - a 
        maxwell distribution of energies in eV.
        The attribute is produced using the number_gen class.

        Parameters
        ------
        max_boltz_eV : a max_boltz_eV object.
        """
        self.energy_select = number_gen(max_boltz.intensity, max_boltz.domain)
        self.energy_select.normalise()
        
    def max_dist_photoelectron(self, photon, electron):
        """Method of assigning new x, y, theta, phi and energy attributes to 
        photoelectron assuming it's emitted from the photocathode with cos^6 
        dispersion and maxwell distributed energy after an incident photon 
        hits the photocathode.

        Parameters
        ------
        photon: photon object
            Photon defined with a position, trajectory and wavelength.
        electron: electron object.
            Electron defined with default attributes.
        """
        if photon.exist == True:
            electron.x = photon.x
            electron.y = photon.y
            electron.t = photon.t
            theta_dist = number_gen(cos_6, (0, math.pi/2))
            electron.theta = theta_dist.dice_roll()
            electron.phi = random.uniform(0, 2*math.pi)
            electron.energy = photon.energy - float(self.energy_select.dice_roll())
            if electron.energy > 0:
                if random.rand() < self.quantum_efficiency(photon.wavelength):
                    electron.exist = True
                else:
                    electron.exist = False
        
        
    def photoelectron(self, photon, electron):
        """Method of assigning new x, y, y, theta, phi, energy attributes to 
        photoelectron assuming it's emitted from the photocathode with cos^6 
        dispersion and using its uniform work function work_func.
        
        Parameters
        ------
        photon: photon object
            Photon defined with a position, trajectory and wavelength.
        electron: electron object
            Electron defined with default attributes.
        """
        if photon.exist == True:
            electron.x = photon.x
            electron.y = photon.y
            electron.t = photon.t
            
            theta_dist = number_gen(cos_6, (0, math.pi/2))
            electron.theta = theta_dist.dice_roll()
            electron.phi = random.uniform(0, 2*math.pi)
            electron.energy = photon.energy - self.work_func
            if electron.energy > 0:
                if random.rand() < self.quantum_efficiency(photon.wavelength):
                    electron.exist = True
                else:
                    electron.exist = False
                
    def read_response(self, filename):
        """Method of taking a .csv file with responsivity data.
        csv file should have first row as column headers; wavelength
        (nm) - first column, responsivity (mA/W) - second column.
        Re-defines object quantum_efficiency method as a cubic 
        spline interpolated function of wavelength.
        """
        h = 6.626e-34
        c = 3e8
        e = 1.6e-19
        scale = 1e6
        
        wavelength = []
        response = []
        
        data = csv.reader(open(filename, 'r'))
        x = 0
     
        for row in data:
            
            if x > 0:
                
                wavelength.append(float(row[0]))
                response.append(float(row[1]))
            x += 1
            
        wavelength = np.asarray(wavelength)
        response = np.array(response)
        
        quantumefficiencies = scale * h * c * response / (wavelength * e)
        
        self.quantum_efficiency = interpolate.interp1d(wavelength, 
                                                       quantumefficiencies, 
                                                       kind = 'cubic') 
                                                       
                     
class MCP(plate):
    """A plate defined by MCP characteristics.
    
    Parameters
    ------
    MCPout_V, MCPin_V : float
        the electrical potential on each face of the MCP
    alpha : float
        alpha ratio of MCP
    beta : float
        beta ratio of MCP
    diameter : float
        Channel diameter of MCP

    Attributes
    ------
    e_mass : float
        electron mass in kg
    e_charge : float
        electron charge in C
    distance_scale : float
        10^-3 corresponding to mm distances
        
    Notes
    ------
    When initialised, the attribute 'number_dynodes' is set to the ratio
    alpha/beta.  Under Eberhardt's assumptions, the gain resulting from a micro-
    -channel can be treated as if it were caused by a chain of a fixed number of 
    dynodes [1].  The potential difference between each dynode is assigned to 
    the attribute 'pd_section'.
    
    The attribute 'radial_emit' is assigned the value of (dynode pd) / (4*alpha*beta)
    under the assumption that the average radial emission energy of electrons from 
    a MCP is proportional to the primary bombarding energy [1].  Furthermore, the 
    attribute 'theta_disperse' is the average MCP emissive electron theta angle.
    It is assigned the value of the square-root-ratio of the radial emission energy 
    and axial emission energy.  This follows from a small angle approximation of 
    arctan(x) and also assumes that any secondary electrons emitted from the final 
    dynode only have axial energy given by the pd between adjacent dynodes.
    
    The attribute 'dynode_time' follows the formula laid out by Eberhardt's
    previous assumption, and is the time taken for electrons to travel between 
    adjacent dynodes [1].
    
    The attributes radial_rayleigh and min_E are crucial for defining emission from the MCP
    out plate in terms of the radial component of a rayleigh distributed emission energy [2].
    The method electron_gain_stat3() provides the functionality to assign an emitting spotlight
    such a dispersion relation.
    
    References
    ------
    .. [1] E. Eberhardt, "Gain model for microchannel plates", Applied Optics, 
    vol. 18, no. 9, pp.1418 - 1423, 1979
    
    .. [2] Influence of the microchannel plate and anode gap parameters on the
        spatial resolution of an image intensifier, T. H. Hoenderken et al., 
        American Vacuum Society, Journal of Vacuum Science and Technology B,
        vol. 19, no. 3, May/June 2001
    """
    e_mass = 9.11e-31
    e_charge = 1.6e-19
    distance_scale = 1e-3

    def __init__(self, dimension_array, MCPout_V, MCPin_V, alpha, beta, 
                 diameter, radial_rayleigh):
        super().__init__(dimension_array)
        
        self.MCPout_V = MCPout_V
        self.MCPin_V = MCPin_V
        self.voltage = MCPout_V - MCPin_V
        
        self.alpha = alpha
        self.beta = beta
        self.diameter = diameter
        
        self.number_dynodes = alpha / beta
        self.pd_section = (MCPout_V - MCPin_V)/self.number_dynodes
        
        a = 2*self.e_mass*self.alpha*self.beta
        b = self.e_charge*self.voltage
        self.dynode_time = self.distance_scale * diameter * (a/b)**0.5
        
        self.radial_emit = self.pd_section / (4 * self.alpha * self.beta)
        
        self.theta_disperse = np.sqrt(1 / (4 * self.alpha * self.beta))
        
        self.radial_rayleigh = radial_rayleigh
        self.min_E = np.amin(radial_rayleigh.bin_centres)
    
    def gaussian_gain_dist(self, gaussian):
        """
        Method to save a MCP electron gain distribution. Method also defines 
        mean gain, standard deviation and the average gain per stage.
        
        Parameters
        ------
        gaussian : a gaussian object
        """
        self.gain = gaussian.random
        self.gain_mu = gaussian.mu
        self.gain_sigma = gaussian.sigma
        self.gain_per_stage = gaussian.mu **(-self.number_dynodes)
        
    def electron_gain_discreet(self, electron, spotlight):
        """
        Method to populate a spotlight object's electron dispersion
        characteristic. Method produces sets of data defining each discreet 
        electron's state.
        (Not advised and obselete)
        """
        
        if electron.exist == True:
            spotlight.exist = True
            spotlight.gain = round(self.gain())
            spotlight.x_centre = electron.x
            spotlight.y_centre = electron.y
            spotlight.phi = []
            spotlight.theta = []
            spotlight.r_coords = []
            spotlight.energies = []
            spotlight.times = []

            for i in range(spotlight.gain):
                
                spotlight.times.append(electron.t)
            
                phi = random.uniform(0, 2*np.pi)
                spotlight.phi.append(phi)
            
                radial_energy = self.radial_dist.random()
            
                spotlight.theta.append(np.sqrt(radial_energy / self.pd_section))
                spotlight.r_coords.append(0)
            
                spotlight.energies.append(self.pd_section + radial_energy)
                
    def electron_gain_stat1(self, electron, spotlight):
        """
        A method to define spotlight object's characteristic electron gain
        and dispersion using statistical representations.
        
        Parameters
        ------
        electron : an electron object
            MUST be defined with a position.
        spotlight : a spotlight object
            Assumes it is with defualt attributes.
            
        Notes
        ------
        The electron gain through an MCP plate is approximately modelled
        as a Gaussian distribution.
        
        The dispersion of electrons emitted by the MCP after the final 
        'dynode' is here treated like a Gaussian, where a spotlight is given the
        attributes 'spotlight.radial_mean_energy', 'spotlight.axial_mean_energy' 
        and a 'mean_theta' determined by the attributes 'self.radial_emit',
        'self.pd_section' and 'self.theta_disperse' respectively.  The Gaussian
        dispersion characteristic of the spotlight is assigned a mean theta
        angle of 0 rad and standard deviation equal to the 'mean_theta' angle. 
        While a departure from the literature outlined by Eberhardt, it does allow
        the angle to be easily modelled as a circularly symmetrical Gaussian. 
        NB: This may subtley change in future.
        """
        if electron.exist == True:
            spotlight.exist = True
            spotlight.gain = round(self.gain())
            spotlight.x_centre = electron.x
            spotlight.y_centre = electron.y
            spotlight.time = electron.t + self.dynode_time * self.number_dynodes
            
            spotlight.radial_mean_energy = self.radial_emit
            spotlight.axial_mean_energy = self.pd_section
            spotlight.mean_theta = self.theta_disperse

            spotlight.theta = gaussian(0, spotlight.mean_theta)
            spotlight.theta.domain = (0, np.pi/2)  
            
    def electron_gain_stat2(self, electron, spotlight):
        """
        A method to define spotlight object's characteristic electron gain
        and dispersion using statistical representations.
        
        Parameters
        ------
        electron : an electron object
            MUST be defined with a position.
        spotlight : a spotlight object
            Assumes it is with defualt attributes.
            
        Notes
        ------
        The electron gain through an MCP plate is approximately modelled
        as a Gaussian distribution.
        
        The dispersion of electrons emitted by the MCP after the final 
        'dynode' is here treated using a rayleigh distribution of energies,
        assuming that this energy is largely directed in the radial direction [2].
        
        References
        ------
        .. [2] Influence of the microchannel plate and anode gap parameters on the
        spatial resolution of an image intensifier, T. H. Hoenderken et al., 
        American Vacuum Society, Journal of Vacuum Science and Technology B,
        vol. 19, no. 3, May/June 2001
        
        """
        if electron.exist == True:
            spotlight.exist = True
            spotlight.gain = round(self.gain())
            spotlight.x_centre = electron.x
            spotlight.y_centre = electron.y
            spotlight.time = electron.t + self.dynode_time * self.number_dynodes
            
            rayleigh_energies = rayleigh()
            energy_select = number_gen(rayleigh_energies.distribution, (0, 3))
            energy_select.normalise()
            
            spotlight.radial_energy = energy_select.dice_roll
            
    def electron_gain_stat3(self, electron, spotlight):
        """
        A method to define spotlight object's characteristic electron gain
        and dispersion using statistical representations.
        
        Parameters
        ------
        electron : an electron object
            MUST be defined with a position.
        spotlight : a spotlight object
            Assumes it is with defualt attributes.
            
        Notes
        ------
        The electron gain through an MCP plate is approximately modelled
        as a Gaussian distribution.
        
        The dispersion of electrons emitted by the MCP after the final 
        'dynode' is here treated using a rayleigh distribution of energies,
        projecting the energy in the radial direction with a sine distribution of 
        theta.
        Method assumes axial energy is negligible compared to anode gap pd [2].
        
        References
        ------
        .. [2] Influence of the microchannel plate and anode gap parameters on the
        spatial resolution of an image intensifier, T. H. Hoenderken et al., 
        American Vacuum Society, Journal of Vacuum Science and Technology B,
        vol. 19, no. 3, May/June 2001
        
        """
        if electron.exist == True:
            spotlight.exist = True
            spotlight.gain = round(self.gain())
            spotlight.x_centre = electron.x
            spotlight.y_centre = electron.y
            spotlight.time = electron.t + self.dynode_time * self.number_dynodes
            
                     
            energy_select = number_gen(self.radial_rayleigh.radial_energy, 
                                       (self.min_E, 3))
            energy_select.normalise()
                                         
            spotlight.radial_energy = energy_select.dice_roll

            
class gap:
    """Class for defining traversal gaps for particle objects.
    
    Parameters
    ------
    separation : float
        The distance of the gap in millimetres mm.
    """
    def __init__(self, separation):
        self.separation = separation
        
    def traj_zdist_simple(self, particle):
        """Calculates a new x and y position for a particle after travelling
        the gap - no acceleration.
        
        Parameters
        ------
        particle : a particle object
        
        Notes
        ------
        The trajectory is calculated using trigonometry of the particle.theta
        and particle.phi angles with the gap distance.
        """
        r = self.separation*np.tan(particle.theta)
        particle.x += r*np.cos(particle.phi)
        particle.y += r*np.sin(particle.phi)
    
    
class cathode_gap:
    """Class for describing a photocathode gap between a cathode out and MCP_in.

    Parameters
    ------
    MCPin_V: float
        The volatage on the MCP_in plate in volts.
    cath_V: float
        The voltage on the photocathode plate in volts.
    separation: float
        The distance of the gap in millimetres, mm.
    """
    def __init__ (self, MCPin_V, cath_V, separation):
        self.MCPin_V = MCPin_V
        self.cath_V = cath_V
        self.separation = separation
        
    def traj_cathode_gap(self, electron):
        """Method for calculating a new position x,y, trajectory theta, phi,
        energy and time attributes for an electron traversing the cathode gap.
        Assumes that electron initially has much much more energy directed in 
        radial (x, y) direction than the axial direction.
        
        Parameters
        ------
        electron : an electron object
        
        Notes
        ------
        The traj_cathode_gap() method assumes that an electron traversing the gap
        has much more energy in the radial direction than the axial direction.  This 
        means that the traversal time is Taylor approximated as separation*sqrt(2m/eV)
        where m and e are electron mass and charge respectively, and V is the cathode gap
        pd [2].
        The radial displacement is also approximated according to r = 2*L*sqrt(V_r/V)
        where V_r is electron energy (assumed to be radial) [2]
        
        References
        ------
        .. [2] E. Eberhardt, "Image transfer properties of proximity focused image 
        tubes", Applied Optics, vol.16, no. 8, 1977
        """
        if electron.exist == True:
            V = self.MCPin_V - self.cath_V
            electron.theta = np.arctan(np.sqrt(electron.energy/V))
            electron.energy += V
            a = 2 * electron.mass/(electron.charge * V)
            electron.t += self.separation * 10**(-3) * np.sqrt(a)
        
            radial_shift = 2*self.separation*np.sqrt(electron.energy/V)
            electron.x +=radial_shift*np.cos(electron.phi)
            electron.y +=radial_shift*np.sin(electron.phi)

                       
class anode_gap:
    """Class for describing a photocathode gap

    Parameters
    ------
    MCPin_V: float 
        The volatage on the MCP_in plate in volts.
    cath_V: float
        The voltage on the photocathode plate in volts.
    separation: float
        The distance of the gap in millimetres, mm.
        
    Attributes
    ------
    scale : float
        10^3 for mm scale distances
    """
    scale = 1e3
    def __init__ (self, anode_V, MCPout_V, separation):
        self.MCPout_V = MCPout_V
        self.anode_V = anode_V
        self.separation = separation
        
    def traj_anode_gap_stat(self, spotlight):
        """
        Placeholder description
        
        Parameters
        ------
        spotlight : a spotlight object
        """
        spotlight.traversed_distance = self.separation
        spotlight.axial_energy = self.anode_V - self.MCPout_V
        
    def traj_anode_gap_discreet(self, spotlight):
        """Method for calculating new positions x,y,r and trajectory 
        theta, phi, and energy and time for electrons in a spotlight 
        cone traversing the anode gap. This uses the discreet
        defined spotlight (not recommended).
        
        Parameters
        ------
        spotlight : a spotlight object
            spotlight with data set representing discreet electons
        """
        if spotlight.exist == True:
            energies = np.asarray(spotlight.energies)
            theta = np.asarray(spotlight.theta)
            phi = np.asarray(spotlight.phi)
            times = np.asarray(spotlight.times)
            
            axial_energies = energies * np.cos(theta)**2
            axial_speeds = self.scale * np.sqrt(2 * axial_energies * spotlight.charge / spotlight.mass)
            radial_energies = energies * np.sin(theta)**2
            
            x_coord_energies = radial_energies * np.cos(phi)**2
            x_speeds = self.scale * np.sqrt(2 * x_coord_energies * spotlight.charge / spotlight.mass)
            
            y_coord_energies = radial_energies * np.sin(phi)**2
            y_speeds = self.scale * np.sqrt(2 * y_coord_energies * spotlight.charge / spotlight.mass)
            
            new_axial_energies = axial_energies+ np.full((1, len(axial_energies)),
                                                         self.anode_V - self.MCPout_V)
            new_axial_speeds = self.scale * np.sqrt(2 * new_axial_energies * spotlight.charge / spotlight.mass)
            
            transit_times = 2 / (axial_speeds + new_axial_speeds) * self.separation
            spotlight.times = [*transit_times]
            
            x_coords = transit_times * x_speeds
            y_coords = transit_times * y_speeds
            
            spotlight.x_coords = [*x_coords]
            spotlight.ycoords = [*y_coords]
            
            energies = energies + new_axial_energies
            spotlight.energies = [*energies]
            
            spotlight.theta = []
            spotlight.phi = []
            
            
class spotlight:
    """
    A class to define an emission "cloud" of electrons as a spotlight.
    
    Attributes
    ------
    mass : float
        electron mass in kg.
    charge : float
        electron charge in C.
    exist : bool
        Determine's whether spotlight exists for object interactions
        and certain class methods.
    traversed_distance : float
        Attribute defines the distance the spotlight has "shone";
        essentially defines a projected length for the dispersion 
        attributes of object.
    gain : float
        The electron gain conatined within the spotlight
    """
    mass = 9.11e-31
    charge = 1.6e-19
    exist = False
    traversed_distance = 0
    gain = 1
    phi = 0
    radial_energy = 0
    axial_energy = 0
    
    def angular_distribution1(self, bins=50, **kwargs):
        """
        A method to display the angular distribution of dispersed electrons.
        Displays a 2D histogram with theta angle horizontally, and phi vertically
        using matplotlib.pyplot. For use with spotlights that had
        MCP.electron_gain_stat1() applied to them.
        
        Parameters
        ------
        bins : float, optional
        kwargs : optional
        """
        if self.exist == True:
            theta = np.empty(100)
            phi = np.empty(100)
        
            for i in range(100):
                theta[i] = self.theta.random()
                phi[i] = random.uniform(0, 2*np.pi)
                
            hist, xedge, yedge = np.histogram2d(theta, 
                                            phi, bins)
            plt.figure()
            plt.imshow(hist, **kwargs)
            plt.show()
            
        else: 
            print("Spotlight doesn't exist") 
            
    
    def spacial_distribution1(self, bins=50, **kwargs):
        """Displays a x, y distribution of spotlight dispersion as a 2-D colour 
        histogram using matplotlib.pyplot. For use with spotlights that had
        MCP.electron_gain_stat1() applied to them.
        
        Parameters
        ------
        bins : float, optional
        kwargs : optional
        """
        if self.exist == True:
            theta = np.empty(100)
            phi = np.empty(100)
        
            for i in range(100):
                theta[i] = self.theta.random()
                phi[i] = random.uniform(0, 2*np.pi)
            
            self.x_coords = self.traversed_distance * theta * np.cos(phi)
            self.y_coords = self.traversed_distance * theta * np.sin(phi)
            
            self.x_min = np.amin(self.x_coords)
            self.x_max = np.amax(self.x_coords)
            self.y_min = np.amin(self.y_coords)
            self.y_max = np.amax(self.y_coords)
        
            hist, xedge, yedge = np.histogram2d(self.x_coords, 
                                            self.y_coords, bins)
            plt.figure()
            plt.imshow(hist, extent = [self.x_min, self.x_max,
                                       self.y_min, self.y_max], **kwargs)
            plt.show()
            
        else:
            print("Spotlight doesn't exist")
            
            
    def angular_distribution2(self, bins=50, **kwargs):
        """
        A method to display the angular distribution of dispersed electrons.
        Displays a 2D histogram with theta angle horizontally, and phi vertically
        using matplotlib.pyplot. For use with spotlights that had
        MCP.electron_gain_stat2(), and anode_gap.traj_anode_gap_stat() applied 
        to them.
        
        Parameters
        ------
        bins : float, optional
        kwargs : optional
        
        Notes
        ------
        Method assumes that dispersed electrons have traversed an anode gap
        with negligible or zero initial axial energy [2].
        
        References
        ------
        .. [2] E. Eberhardt, "Image transfer properties of proximity focused image 
        tubes", Applied Optics, vol.16, no. 8, 1977
        """
        if self.exist == True:
            theta = np.empty(1000)
            phi = np.empty(1000)
        
            for i in range(1000):
                radial_energy = self.radial_energy()
                theta[i] = 2 * np.sqrt(radial_energy / self.axial_energy)
                phi[i] = random.uniform(0, 2*np.pi)
                
            self.hist, self.xedge, self.yedge = np.histogram2d(theta, 
                                            phi, bins)
            plt.figure()
            plt.imshow(self.hist, **kwargs)
            plt.show()
            
        else: 
            print("Spotlight doesn't exist") 
            
    
    def spacial_distribution2(self, bins=50, **kwargs):
        """Displays a x, y distribution of spotlight dispersion as a 2-D colour 
        histogram using matplotlib.pyplot. For use with spotlights that had
        MCP.electron_gain_stat2(), and anode_gap.traj_anode_gap_stat() applied
        to them.
        
        Parameters
        ------
        bins : float, optional
        kwargs : optional
        
        Notes
        ------
        Method assumes that dispersed electrons have traversed an anode gap with
        negligible or zero initial axial energy [2].
        
        References
        ------
        .. [2] E. Eberhardt, "Image transfer properties of proximity focused image 
        tubes", Applied Optics, vol.16, no. 8, 1977
        """
        if self.exist == True:
            radius = np.empty(1000)
            phi = np.empty(1000)
        
            for i in range(1000):
                radial_energy = self.radial_energy()
                theta = 2 * np.sqrt(radial_energy / self.axial_energy)
                radius[i] = self.traversed_distance * theta
                phi[i] = random.uniform(0, 2*np.pi)
            
            self.x_coords = radius * np.cos(phi)
            self.y_coords = radius * np.sin(phi)
            
            self.x_min = np.amin(self.x_coords)
            self.x_max = np.amax(self.x_coords)
            self.y_min = np.amin(self.y_coords)
            self.y_max = np.amax(self.y_coords)
        
            self.hist, self.xedge, self.yedge = np.histogram2d(self.x_coords, 
                                            self.y_coords, bins)
            plt.figure()
            plt.imshow(self.hist, extent = [self.x_min, self.x_max,
                                       self.y_min, self.y_max], **kwargs)
            plt.show()
            
        else:
            print("Spotlight doesn't exist")
            

class anode(plate):
    """
    A plate sub class which can specifically capture a spotlight object defined 
    with binned x,y data to be mapped onto the anode screen.
    
    Parameters:
    ------
    pixel_dim: Tuple
        A tuple containing integer values (X, Y) for the horizontal and vertical
        dimensionality of the plate in terms of the number of pixels
        
    Attributes:
    ------
    hits: list
        Catalogues the number of electron hits for a given x,y position on the screen.
    """
    def __init__(self, dimension_array, pixel_dim):
        super().__init__(dimension_array)
        self.hits = []
        self.pixel_dim = pixel_dim
        
    def spotlight_hit(self, spotlight):
        """
        A method to map a spotlight object's spacial_distribution onto the anode.
        
        Notes:
        ------
        The spotlight's histogram is used to calculate expectation values for the 
        number of particles at mapped (x,y) bin centre points from spotlight to anode.
        """
        if spotlight.exist == True:
            probability = spotlight.hist / np.sum(spotlight.hist)
            total = spotlight.gain
            number_hit = total * probability
            x_centres = np.mean(np.vstack([spotlight.xedge[0:-1],
                                           spotlight.xedge[1:]]), axis=0)
            
            y_centres = np.mean(np.vstack([spotlight.yedge[0:-1], 
                                           spotlight.yedge[1:]]), axis=0)
            x_centres += spotlight.x_centre
            
            y_centres += spotlight.y_centre
            
            for i in range(len(x_centres)):
                for j in range(len(y_centres)):
                    if self.x_min < x_centres[i] < self.x_max:
                        if self.y_min < y_centres[j] < self.y_max:
                            self.x_coords.append(x_centres[i])
                            self.y_coords.append(y_centres[j])
                            self.hits.append(number_hit[i, j])
                            
    def pixelise(self, **kwargs):
        """
        A method to map the object's x_coords, y_coords and hits into a pixelised
        data according to pixel_dim. Defines a new attribute - array of data which 
        have dimensions according to pixel_dim and contains the hit data. 
        
        
        Returns:
        ------
        A matplotlib.pyplot image plotting the pixelated data as a 2d histogram
        
        """
        pixel_length = (self.x_max - self.x_min)/self.pixel_dim[0]
        pixel_width = (self.y_max - self.y_min)/self.pixel_dim[1]
        
        self.x_coords_pix = (self.x_coords - self.x_min) // pixel_length
        self.y_coords_pix = (self.y_coords - self.y_min) // pixel_width
        
        self.hist, self.xedges, self.yedges = np.histogram2d(self.x_coords_pix,
                                                             self.y_coords_pix,
                                                             weights = self.hits)
        plt.figure()
        plt.imshow(self.hist, extent = [self.x_min, self.x_max,
                                       self.y_min, self.y_max], **kwargs)
        plt.colorbar()
        plt.show() 
   

class max_boltz:
    """Class for defining a maxwell boltzmann energy distribution.
    
    Parameters
    ------
    temperature : float
        blackbody temperature defined in kelvin K.
        
    Attributes
    ------
    domain : tuple
        Defines (energy_minimum, energy_maximum) for the distribution.
    k : float
        Boltzmann constant defined in eV/K.
    amplitude : float
        a scaling factor for the energy distribution.
    """
    domain = (0, 1)
    k = 8.617e-5 
    amplitude = np.sqrt(2/np.pi)
    
    def __init__(self, temperature):
        self.temperature = temperature
        self.a = np.sqrt(self.temperature * self.k /2)
        
        
    def intensity(self, E):
        """Method which returns the intensity of an energy in the max_boltz 
        distribution in arbitary units defined by scaling set by amplitude
        
        Parameters
        ------
        E : float 
            energy in eV
 
        Returns
        ------
        float 
        Probability between 0, 1.
        """
        return self.amplitude * E**2 * np.exp(-E**2 / (2*self.a**2) ) / self.a

            
class blackbody:
    """Class to define a blackbody.
    Parameters
    ------
    temperature : float
        temperature measured in Kelvin. 
        
    Attributes
    ------
    domain : tuple 
        defines (wavelength_minimum, wavelength_maximum) measured in
        nanometres nm.
    h : float
        Planck's constant in Js
    c : float
        speed of light in m/s
    k : float
        Boltzmann constant in J/K
        
    """
    domain = (220, 1e3)
    h = 6.626e-34
    c = 3e8
    k = 1.38e-23
    def __init__(self, temperature):
        self.temperature = temperature
        self.a = 2.0*self.h * self.c**2 * 1e9
    
    def intensity(self, wavelength):
        """Method for returning wavelength intensity of the blackbody.

        Parameters
        ------
        wavelength: float
            wavelength in nano-metres (nm).

        Returns:
        float 
        """
        self.b = self.h * self.c * 1e9 /(wavelength * self.k * self.temperature)
    
        return self.a/ ( (wavelength**5) * (np.exp(self.b) - 1.0) )
    
    
class gaussian:
    """
    A class for defining a gaussian distribution.
    
    Parameters
    ------
    mu : float
        average value for gaussian
    sigma : float
        standard deviation
    """
    def __init__(self, mu, sigma):
        self.mu = mu
        self.sigma = sigma 
        self.domain = (mu - 3*sigma, mu + 3*sigma)
    
    def random(self):
        """A method for returning a gaussian random number within domain.
        Uses self.domain to define bounds of random number selection.
        """
        while True:
            x = random.normal(self.mu, self.sigma)
            if self.domain[0] < x < self.domain[1]:
                break
        return x
    

class rayleigh:
    """
    Class for defining a rayleigh distribution (of energies eV).
    
    Parameters
    ------
    chi : float, optional
        Defines the peak of the rayleigh distribution. Default is 2
        for peak of sqrt(2)/2 eV.
    """
    def __init__(self, chi=2):
        self.chi = chi
        
    def distribution(self, energy):
        return energy/self.chi * np.exp(-energy**2/(2*self.chi))
    
    
class radial_rayleigh:
    """
    Class for defining a distribution (of energies eV) in the radial
    direction for an MCP. Initialises a set of random numbrs to Monte
    Carlo generate grouped data for interpolation by cubic splines.
    This interpolation acts as a function for use with number_gen class.
    """
    def __init__(self):
        rayleigh1 = rayleigh()
        rayleigh_dist = number_gen(rayleigh1.distribution, (0, 4))
        rayleigh_dist.normalise()
        cos_dist = number_gen(cos, (0, np.pi/2))
        cos_dist.normalise()
        
        energies = rayleigh_dist.dice_roll(100000)
        E = np.asarray(energies)
        angles = cos_dist.dice_roll(100000)
        A = np.asarray(angles)

        radial_energy = E * np.sin(A)**2

        self.hist, self.bin_edges = np.histogram(radial_energy, bins = 500)
        self.bin_centres = np.mean(np.vstack([self.bin_edges[0:-1], self.bin_edges[1:]]), axis=0)
        
        self.radial_interp_energy = interpolate.interp1d(self.bin_centres, self.hist, kind='cubic')
        
    def radial_energy(self, energy):
        return self.radial_interp_energy(energy)
    

def cos(theta):
    return np.cos(theta)
            
            
def cos_6(theta):
    """Function which returns cos^6(theta).
    
    Parameters
    ------
    theta : float
    """
    return np.cos(theta)**6
